#!/usr/bin/python
"""
Intended as a post-commit hook in a git repository.

Uploads commit details as json to a server via REST.
"""

import git
import requests
import json
import traceback, os

#TODO: put in external config file, or use git.config
POST_URL = "http://127.0.0.1:5000/commit-log"

def read_commit_data():
	"""
	:returns: dictionary containing all required commit details.
	"""
	repo   = git.Repo(".")		#Todo: deal with working path
	head   = repo.head
	commit = head.commit
	branch = head.reference
	
	commiter  = commit.author
	
	repo_name = repo.git_dir if repo.bare else repo.working_dir 
	
	file_list = commit.stats.files.keys()
	
	diffs     = {}
	if commit.parents:
		diff_vs   = commit.parents[0]
		for f in file_list:
			#Extract the patches from the change set:
			diffs[f] = map(lambda d: d.diff, commit.diff(diff_vs, paths=f, create_patch=True))
	else:
		#Special case for the initial commit..
		#TODO: format the strings in the patch format.
		#TODO: this assumes the file has not changed on disk since the commit;
		#      imporve this by reading the correct blob from the commited tree
		for f in file_list:
			with open(f) as new_file:
				diffs[f] = new_file.read()
	
	#The returned dictionary:
	class Bogo:
		pass
	return_data = Bogo()
	return_data.commiter       = str(commiter)
	return_data.commmiter_mail = str(commiter.email)
	return_data.repo_name      = str(repo_name)
	return_data.file_list      = file_list
	return_data.diffs          = diffs
	return_data.commit_message = str(commit.message)
	return_data.commit_date    = commit.committed_date
	
	return return_data.__dict__

def post_commit_to_overloards(commit_dict):
	"""
	Uploads the provided dictionary to the hardcoded RESTful url as json
	"""
	r = requests.post(POST_URL, data=json.dumps(commit_dict), headers={'content-type':'application/json'})
	return r.text


def main():
	commit_dict = read_commit_data()
	server_out  = post_commit_to_overloards(commit_dict)
	
	print "Server returned: %s\n"%server_out
	print "Done Notify Overloards."


if __name__ == '__main__':
	try:
		main()
	except KeyboardInterrupt:
		os._exit(-1)		#abruptly terminate all threads
		raise
	except Exception as e:
		traceback.print_exc()
		os._exit(-1)		#abruptly terminate all threads
		raise
